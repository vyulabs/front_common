package interfaces

type Country struct {
	Name				string
	Code				string
	PhoneCode			string
	RegionMap			map[string]*Region
}

type Region struct {
	Name				string
	Code				string
}

type RegionProvider interface {
	GetCountries() []*Country
	GetCountry(countryCode string) *Country
	GetCountryName(countryCode string) *string
	GetCountryPhoneCode(countryCode string) *string
	GetRegion(countryCode string, regionCode string) *Region
	GetRegionName(countryCode string, regionCode string) *string
	SplitPhoneNumber(phoneNumber string) (string, string)
}
