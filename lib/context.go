package lib

import (
	"bitbucket.org/vyulabs/front_common/lib/interfaces"
	"net/http"
)

func ContextMiddleware(contextProvider interfaces.ContextProvider) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Header == nil {
				next.ServeHTTP(w, r)
				return
			}

			r, _ = contextProvider.SaveContext(r)

			next.ServeHTTP(w, r)
		})
	}
}