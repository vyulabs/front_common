package azure

import (
	"log"
	"net/url"
	"os"
	"strings"
)

type AzureConfig struct {
	ResourceGroupName string
	StorageAccount    string
	StorageAccessKey  string

	CampaignBucket string
	UserBucket     string
	S3Bucket       string
	LogBucket      string
	TempBucket     string
	BackupBucket   string

	CdnProfileName string
}

type Azure struct {
	Config *AzureConfig

	log		*log.Logger
}

//
// Constructor
//

func NewAzure(config *AzureConfig) (*Azure, error) {
	log := log.New(os.Stderr, "Azure ", log.Lmsgprefix)

	return &Azure{
		Config: config,
		log:	log,
	}, nil
}

//
// Path convert
//

func (rc *Azure) PathToUrl(
	bucket string,
	path string,
) string {
	return "https://" + rc.Config.StorageAccount + ".blob.core.windows.net/" + bucket + "/" + path
}

func (rc *Azure) DirectUrlToCdnUrl(directUrl string) (string, error) {
	if directUrl == "" {
		return directUrl, nil
	}

	urlObj, urlErr := url.Parse(directUrl)
	if urlErr != nil {
		return "", urlErr
	}

	hostParts := strings.Split(urlObj.Host, ".")
	if len(hostParts) < 1 {
		return directUrl, nil
	}

	if !strings.HasSuffix(urlObj.Host, "windows.net") {
		return directUrl, nil
	}

	cdnUrl := "https://" + rc.Config.CdnProfileName + ".azureedge.net" + urlObj.Path
	return cdnUrl, nil
}

func (rc *Azure) PathToCdnUrl(
	bucket string,
	path string,
) (string, error) {
	return rc.DirectUrlToCdnUrl(rc.PathToUrl(bucket, path))
}
