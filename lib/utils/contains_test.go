package utils

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestContains(t *testing.T) {

	t.Run("#Contains should return true if list contains item", func(t *testing.T) {
		require.Equal(t, true, Contains([]string{"abc"}, "abc"))
		require.Equal(t, true, Contains([]string{"abc", "def", "ghi"}, "def"))

		require.Equal(t, true, Contains([]int{8}, 8))
		require.Equal(t, true, Contains([]int{6, 8, 1}, 8))

		require.Equal(t, true, Contains([]int32{8}, 8))
		require.Equal(t, true, Contains([]int32{6, 8, 1}, 8))

		require.Equal(t, true, Contains([]int64{8}, 8))
		require.Equal(t, true, Contains([]int64{6, 8, 1}, 8))

		require.Equal(t, true, Contains([]float32{8.8}, 8.8))
		require.Equal(t, true, Contains([]float32{6.6, 8.8, 1.1}, 8.8))

		require.Equal(t, true, Contains([]float64{8.8}, 8.8))
		require.Equal(t, true, Contains([]float64{6.6, 8.8, 1.1}, 8.8))
	})

	t.Run("#Contains should return false if list doesn't contains item", func(t *testing.T) {
		require.Equal(t, false, Contains([]string{"abc", "def", "ghi"}, "jkl"))
		require.Equal(t, false, Contains([]string{}, "jkl"))

		require.Equal(t, false, Contains([]int{6, 8, 1}, 9))
		require.Equal(t, false, Contains([]int{}, 9))

		require.Equal(t, false, Contains([]int32{6, 8, 1}, 9))
		require.Equal(t, false, Contains([]int32{}, 9))

		require.Equal(t, false, Contains([]int64{6, 8, 1}, 9))
		require.Equal(t, false, Contains([]int64{}, 9))

		require.Equal(t, false, Contains([]float32{6.6, 8.8, 1.1}, 9.9))
		require.Equal(t, false, Contains([]float32{}, 9.9))

		require.Equal(t, false, Contains([]float64{6.6, 8.8, 1.1}, 9.9))
		require.Equal(t, false, Contains([]float64{}, 9.9))
	})
}
