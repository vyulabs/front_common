package lib

import (
	"bitbucket.org/vyulabs/front_common/lib/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

func StringToPointer(s string) *string {
	if s != "" {
		stringVal := s
		return &stringVal
	}
	return nil
}

func IntToPointer(n int, zeroMeansEmpty bool) *int {
	if n != 0 || !zeroMeansEmpty {
		intVal := n
		return &intVal
	}
	return nil
}

func FloatToPointer(x float64, zeroMeansEmpty bool) *float64 {
	if x != 0 || !zeroMeansEmpty {
		floatVal := x
		return &floatVal
	}
	return nil
}

func TimeToPointer(t time.Time) *string {
	if !t.IsZero() {
		s := utils.TimeToString(t)
		return &s
	}
	return nil
}

func TimePointerToTimeStringPointer(t *time.Time) *string {
	if t != nil {
		s := utils.TimeToString(*t)
		return &s
	}
	return nil
}

func DateToPointer(t time.Time) *string {
	if !t.IsZero() {
		s := utils.TimeToDateString(t)
		return &s
	}
	return nil
}

func TimePointerToDateStringPointer(t *time.Time) *string {
	if t != nil {
		s := utils.TimeToDateString(*t)
		return &s
	}
	return nil
}

func IntPointerToInt64Pointer(p *int) *int64 {
	if p != nil {
		i64 := int64(*p)
		return &i64
	}
	return nil
}

func StringIdToObjectId(stringId string) primitive.ObjectID {
	objectId, _ := primitive.ObjectIDFromHex(stringId)
	return objectId
}

func StringIdsToObjectIds(stringIds []string) []primitive.ObjectID {
	var objectIds []primitive.ObjectID
	for _, stringId := range stringIds {
		objectId := StringIdToObjectId(stringId)
		objectIds = append(objectIds, objectId)
	}
	return objectIds
}

func ObjectIdToStringPointer(id primitive.ObjectID) *string {
	if id.IsZero() {
		return nil
	}

	s := id.Hex()
	return &s
}

func ObjectIdPointerToStringPointer(id *primitive.ObjectID) *string {
	if id == nil {
		return nil
	}

	s := id.Hex()
	return &s
}
