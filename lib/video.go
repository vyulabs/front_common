package lib

import (
	"log"
	"os/exec"
)

func MakeThumbnail(srcPath string, dstPath string) error {
	log.Println("MakeThumbnail - srcPath:", srcPath, ", dstPath: ", dstPath)

	cmd := exec.Command(
		"ffmpeg", "-y", "-i",
		srcPath,
		"-vf", "thumbnail", "-frames:v", "1",
		dstPath,
	)

	log.Println("cmd:", cmd.String())

	return cmd.Run()
}
