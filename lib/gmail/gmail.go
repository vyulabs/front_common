package gmail

import (
	"bitbucket.org/vyulabs/front_common/lib/interfaces"
	"bitbucket.org/vyulabs/front_common/lib/utils"
	"context"
	"encoding/base64"
	"fmt"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/gmail/v1"
	"google.golang.org/api/option"
	"log"
	"net/mail"
	"os"
)

type Gmail struct {
	service *gmail.Service
	log		*log.Logger
}

type Keys struct {
	ClientId		string
	ClientSecret	string
	RefreshToken	string
}

const (
	GmailRedirectUri = "https://developers.google.com/oauthplayground"
	GmailTokenUri = "https://oauth2.googleapis.com/token"
)

// https://developers.google.com/gmail/api/quickstart/go
func NewGmail(keys *Keys) (*Gmail, error) {
	credentials := `{
		"web": {
			"client_id": "` + keys.ClientId + `",
			"client_secret": "` + keys.ClientSecret + `",
			"redirect_uris": ["` + GmailRedirectUri + `"],
			"token_uri": "` + GmailTokenUri + `"
		}
	}`

	config, err := google.ConfigFromJSON([]byte(credentials), gmail.GmailSendScope)
	if err != nil {
		return nil, err
	}

	tok := oauth2.Token{
		RefreshToken: keys.RefreshToken,
	}
	client := config.Client(context.Background(), &tok)

	service, err := gmail.NewService(context.Background(), option.WithHTTPClient(client))

	log := log.New(os.Stderr, "Gmail ", log.Lmsgprefix)

	return &Gmail{
		service: service,
		log: log,
	}, nil
}

// https://stackoverflow.com/questions/25382821/how-to-send-email-through-gmail-go-sdk
func (rc Gmail) SendEmail(email *interfaces.Email) error {
	rc.log.Println("SendEmail:", utils.Stringify(email))

	from := mail.Address{email.From, email.From}
	to := mail.Address{email.To, email.To}

	header := make(map[string]string)
	header["From"] = from.String()
	header["To"] = to.String()
	header["Subject"] = email.Subject
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = "text/plain; charset=\"utf-8\""
	header["Content-Transfer-Encoding"] = "base64"

	var msg string
	for k, v := range header {
		msg += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	msg += "\r\n" + email.Text

	gmsg := gmail.Message{
		Raw: base64.RawURLEncoding.EncodeToString([]byte(msg)),
	}

	_, err := rc.service.Users.Messages.Send("me", &gmsg).Do()
	if err != nil {
		rc.log.Printf("em %v, err %v", gmsg, err)
		return err
	}
	return err
}

func (rc Gmail) SendEmailsAsync(emails []*interfaces.Email) {
	go func() {
		rc.log.Println("SendEmailsAsync start")
		for _, email := range emails {
			err := rc.SendEmail(email)
			if err != nil {
				rc.log.Println("error: " + err.Error())
			}
		}
		rc.log.Println("SendEmailsAsync finish")
	}()
}