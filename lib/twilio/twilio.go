package twilio

import (
	"bitbucket.org/vyulabs/front_common/lib/interfaces"
	"github.com/twilio/twilio-go"
	openapi "github.com/twilio/twilio-go/rest/api/v2010"
	"log"
	"os"
)

const (
	TwilioAccountSidKey 	= "TWILIO_ACCOUNT_SID"
	TwilioAuthTokenKey		= "TWILIO_AUTH_TOKEN"
)

type Twilio struct {
	client 	*twilio.RestClient
	phone	string
	log     *log.Logger
}

type Keys struct {
	AccountSid		string
	AuthToken		string
	Phone			string
}

func NewTwilio(keys *Keys) (*Twilio, error) {
	writeKeysToEnvironment(keys)

	client := twilio.NewRestClient()
	log := log.New(os.Stderr, "Twilio ", log.Lmsgprefix)
	return &Twilio{
		client: client,
		phone: 	keys.Phone,
		log:	log,
	}, nil
}

func (rc *Twilio) SendSms(sms *interfaces.Sms) error {
	rc.log.Println("send SMS to", sms.Phone)
	params := &openapi.CreateMessageParams{
		To: 	&sms.Phone,
		From: 	&rc.phone,
		Body: 	&sms.Text,
	}

	_, sendErr := rc.client.ApiV2010.CreateMessage(params)
	return sendErr
}

func (rc *Twilio) SendSmsesAsync(smses []*interfaces.Sms) {
	go func() {
		rc.log.Println("SendSmsesAsync start")
		for _, sms := range smses {
			err := rc.SendSms(sms)
			if err != nil {
				rc.log.Println("send error:", err.Error())
			}
		}
		rc.log.Println("SendSmsesAsync finish")
	}()
}

//
// utils
//

func writeKeysToEnvironment(keys *Keys) {
	os.Setenv(TwilioAccountSidKey, keys.AccountSid)
	os.Setenv(TwilioAuthTokenKey, keys.AuthToken)
}