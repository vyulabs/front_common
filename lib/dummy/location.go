package dummy

import (
	"bitbucket.org/vyulabs/front_common/lib/interfaces"
)

type DummyLocationProvider struct {
}

func NewDummyLocationProvider() *DummyLocationProvider {
	return &DummyLocationProvider{}
}

func (rc DummyLocationProvider) GetLocationForIpAddress(ipAddress string) (*interfaces.Location, error) {
	return nil, nil
}
