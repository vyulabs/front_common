package dummy

import (
	"golang.org/x/text/language"
)

type DummyTranslationProvider struct {
}

func NewDummyTranslationProvider() *DummyTranslationProvider {
	return &DummyTranslationProvider{}
}

func (rc DummyTranslationProvider) GetTranslations(
	strings []string,
	srcLanguage language.Tag,
	dstLanguage language.Tag,
) ([]string, error) {
	texts := []string{}
	for _, s := range strings {
		texts = append(texts, dstLanguage.String() + ":" + s)
	}

	//fmt.Println(
	//	"GetTranslations - " + srcLanguage.String() + ":" + utils.Stringify(strings) + " -> " +
	//		dstLanguage.String() + ":" + utils.Stringify(texts),
	//)

	return texts, nil
}