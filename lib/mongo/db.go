package mongo

import (
	"bitbucket.org/vyulabs/front_common/lib"
	"bitbucket.org/vyulabs/front_common/lib/db"
	"context"
	"log"
	"os"
	"strings"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Client struct {
	Client *mongo.Client
	log    *log.Logger
}

type MongoDbClient interface {
	GetDb(name string) MongoDataBase
}

func GetDbClient(uri string) MongoDbClient {
	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		log.Fatal(err)
	}

	err = client.Connect(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	log := log.New(os.Stderr, "Mongo ", log.Lmsgprefix)

	return Client{
		Client: client,
		log:    log,
	}
}

func (dbClient Client) GetDb(name string) MongoDataBase {
	dbClient.log.Println("GetDb", name)
	return Database{
		DbName:   name,
		DataBase: dbClient.Client.Database(name),
	}
}

type Database struct {
	DbName   string
	DataBase *mongo.Database
	Context  context.Context
}

type MongoDataBase interface {
	Name() string
	GetDataSource(name string) MongoDataSource
}

func (db Database) Name() string {
	return db.DbName
}

func (db Database) GetDataSource(name string) MongoDataSource {
	return MongoDataSource{
		Name:       name,
		Collection: db.DataBase.Collection(name),
	}
}

func ToMongoId(id db.Id) any {
	if id.IsObjectId {
		return lib.StringIdToObjectId(id.Id)
	}
	return id.Id
}

func GetMongoDatabase(mongoUrl string) MongoDataBase {
	dbUri, dbName := splitMongoUrl(mongoUrl)
	mongoClient := GetDbClient(dbUri)
	return mongoClient.GetDb(dbName)
}

func splitMongoUrl(mongoUrl string) (dbUri string, dbName string) {
	s := mongoUrl
	if strings.Index(s, "mongodb://") == 0 {
		s = s[10:]
	}
	pos := strings.LastIndex(s, "/")
	if pos == -1 {
		return mongoUrl, ""
	}
	return mongoUrl[:pos+10], mongoUrl[pos+11:]
}
