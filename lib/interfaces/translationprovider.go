package interfaces

import "golang.org/x/text/language"

type TranslationProvider interface {
	GetTranslations(ss []string, srcLanguage language.Tag, dstLanguage language.Tag) ([]string, error)
}
