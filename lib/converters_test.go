package lib

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestConverters(t *testing.T) {

	t.Run("#StringToPointer should return nil for empty string", func(t *testing.T) {
		pointer := StringToPointer("")
		require.Equal(t, (*string)(nil), pointer)
	})

	t.Run("#StringToPointer should return pointer to value for non-empty string", func(t *testing.T) {
		pointer := StringToPointer("abc")
		require.Equal(t, "abc", *pointer)
	})

	t.Run("#IntToPointer should return nil for int 0 if zeroMeansEmpty=true", func(t *testing.T) {
		pointer := IntToPointer(0, true)
		require.Equal(t, (*int)(nil), pointer)
	})

	t.Run("#IntToPointer should return pointer to value for int 0 if zeroMeansEmpty=false", func(t *testing.T) {
		pointer := IntToPointer(0, false)
		require.Equal(t, 0, *pointer)
	})

	t.Run("#IntToPointer should return pointer to value for non-empty int", func(t *testing.T) {
		pointer := IntToPointer(123, true)
		require.Equal(t, 123, *pointer)
	})

	t.Run("#FloatToPointer should return nil for float 0 if zeroMeansEmpty=true", func(t *testing.T) {
		pointer := FloatToPointer(0.0, true)
		require.Equal(t, (*float64)(nil), pointer)
	})

	t.Run("#FloatToPointer should return pointer to value for float 0 if zeroMeansEmpty=false", func(t *testing.T) {
		pointer := FloatToPointer(0.0, false)
		require.Equal(t, 0.0, *pointer)
	})

	t.Run("#FloatToPointer should return pointer to value for non-empty float", func(t *testing.T) {
		pointer := FloatToPointer(3.14, true)
		require.Equal(t, 3.14, *pointer)
	})
}