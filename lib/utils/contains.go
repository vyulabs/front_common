package utils

func Contains[T comparable](ss []T, s T) bool {
	for _, x := range ss {
		if x == s {
			return true
		}
	}
	return false
}
