package utils

import (
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
)

func CreateUuid() string {
	return uuid.NewString()
}

func IsInterfaceEmpty(i any) (bool, error) {
	if i == nil {
		return true, nil
	}

	iBson, marshalErr := bson.Marshal(i)
	if marshalErr != nil {
		return false, marshalErr
	}

	iRaw, rawErr := bson.Raw(iBson).Elements()
	if rawErr != nil {
		return false, marshalErr
	}

	return len(iRaw) < 1, nil
}
