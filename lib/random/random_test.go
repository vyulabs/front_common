package random

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestRandom(t *testing.T) {

	t.Run("#RandomString should return random string of requested length", func(t *testing.T) {
		var s string

		s, _ = RandomString(1, 36)
		require.Equal(t, 1, len(s))

		s, _ = RandomString(10, 36)
		require.Equal(t, 10, len(s))

		s, _ = RandomString(100, 36)
		require.Equal(t, 100, len(s))
	})

	t.Run("#RandomString should return random string containing limited set of runes", func(t *testing.T) {
		var s string

		maxIndex := func(s string) int {
			max := 0
			for _, ch := range s {
				index := -1
				for i, _ := range runes {
					if ch == runes[i] {
						index = i
						break
					}
				}
				if max<index {
					max = index
				}
			}
			return max
		}

		s, _ = RandomString(100, 6)
		require.GreaterOrEqual(t, 6, maxIndex(s))

		s, _ = RandomString(100, 16)
		require.GreaterOrEqual(t, 16, maxIndex(s))

		s, _ = RandomString(100, 26)
		require.GreaterOrEqual(t, 26, maxIndex(s))

		s, _ = RandomString(100, 36)
		require.GreaterOrEqual(t, 36, maxIndex(s))
	})
}
