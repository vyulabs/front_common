package lib

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

func GenerateGraphqlSchema(sourceDir string, targetFile string) error {
	log.Println("GenerateGraphqlSchema - sourceDir:", sourceDir, ", targetFile:", targetFile)

	s := ""
	walkErr := filepath.Walk(sourceDir, func(path string, info os.FileInfo, _ error) error {
		if !info.IsDir() {
			s = s + "#\n#\n# " + filepath.Base(path) + "\n#\n#\n\n"
			log.Println("Read file:", path)
			buffer, err := ioutil.ReadFile(path)
			if err != nil {
				return err
			}
			s = s + string(buffer) + "\n\n"
		}
		return nil
	})
	if walkErr != nil {
		return walkErr
	}

	file, fileErr := os.Create(targetFile)
	if fileErr != nil {
		return fileErr
	}

	_, writeErr := file.WriteString(s)
	if writeErr != nil {
		return writeErr
	}

	log.Println("GraphQL schema generated.")
	return nil
}