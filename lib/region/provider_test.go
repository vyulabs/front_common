package region

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestDictionaryRegionProvider(t *testing.T) {

	t.Run("#GetCountries should get countries sorted by name in ascending order", func(t *testing.T) {
		rdp, _ := NewDictionaryRegionProvider()
		countries := rdp.GetCountries()

		require.Equal(t, len(rdp.CountryMap), len(countries))
		for i:=0; i<len(countries)-1; i++ {
			require.Equal(t, true, countries[i+1].Name > countries[i].Name)
		}
	})

	t.Run("#GetCountry should get country", func(t *testing.T) {
		rdp, _ := NewDictionaryRegionProvider()
		country := rdp.GetCountry("RU")

		require.Equal(t, "RU", country.Code)
		require.Equal(t, "Russian Federation", country.Name)
	})

	t.Run("#GetCountryName should get country name", func(t *testing.T) {
		rdp, _ := NewDictionaryRegionProvider()
		countryName := rdp.GetCountryName("RU")

		require.Equal(t, "Russian Federation", *countryName)
	})

	t.Run("#GetCountryPhoneCode should get country phone code", func(t *testing.T) {
		rdp, _ := NewDictionaryRegionProvider()
		countryPhoneCode := rdp.GetCountryPhoneCode("RU")

		require.Equal(t, "+7", *countryPhoneCode)
	})

	t.Run("#GetRegion should get region", func(t *testing.T) {
		rdp, _ := NewDictionaryRegionProvider()
		region := rdp.GetRegion("RU", "CHE")

		require.Equal(t, "CHE", region.Code)
		require.Equal(t, "Chelyabinsk Oblast", region.Name)
	})

	t.Run("#GetRegionName should get region name", func(t *testing.T) {
		rdp, _ := NewDictionaryRegionProvider()
		regionName := rdp.GetRegionName("RU", "CHE")

		require.Equal(t, "Chelyabinsk Oblast", *regionName)
	})

	t.Run("#SplitPhoneNumber should split phone number", func(t *testing.T) {
		rdp, _ := NewDictionaryRegionProvider()
		var code, number string

		code, number = rdp.SplitPhoneNumber("+79671234567")
		require.Equal(t, "+7", code)
		require.Equal(t, "9671234567", number)

		code, number = rdp.SplitPhoneNumber("+61967123456")
		require.Equal(t, "+61", code)
		require.Equal(t, "967123456", number)

		code, number = rdp.SplitPhoneNumber("+37496712345")
		require.Equal(t, "+374", code)
		require.Equal(t, "96712345", number)
	})
}