package db

type FindOptions struct {
	Projection any
	Sort       any
	Limit      *int64
	Skip       *int64
	IgnoreCase *bool
}

type UpdateOptions struct {
	Upsert bool
}

type UpdateResult struct {
	MatchedCount  int64
	ModifiedCount int64
	UpsertedCount int64
	UpsertedId    any
}

type DataSource interface {
	Count(filter map[string]any) (int64, error)

	FindOne(
		filter map[string]any,
		output any,
		opts ...FindOptions,
	) (bool, error)

	FindOneById(
		id Id,
		output any,
		opts ...FindOptions,
	) (bool, error)

	FindMany(
		filter map[string]any,
		output any,
		opts ...FindOptions,
	) error

	Exists(filter map[string]any) (bool, error)

	ExistsById(id Id) (bool, error)

	InsertOne(document any) error

	InsertMany(documents []any) error

	UpdateOneById(
		id Id,
		sets map[string]any,
		unsets map[string]any,
		output any,
		opts ...UpdateOptions,
	) error

	UpdateMany(
		filter map[string]any,
		update map[string]any,
	) (*UpdateResult, error)

	RemoveOneById(id Id) error

	RemoveMany(filter map[string]any) error

	Distinct(
		fieldName string,
		filter map[string]any,
	) (any, error)

	Aggregate(
		pipeline any,
		result any,
	) error
}
