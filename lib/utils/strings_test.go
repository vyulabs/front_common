package utils

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestStrings(t *testing.T) {

	t.Run("#Stringify should stringify object", func(t *testing.T) {
		type Dummy struct {
			Int int
			String string
			Bool bool
			PInt *int
			PString *string
			PBool *bool
		}

		i := 10
		s := "abc"
		b := true

		dummy := Dummy{
			Int: i,
			String: s,
			Bool: b,
			PInt: &i,
			PString: &s,
			PBool: &b,
		}

		dummyStr := Stringify(dummy)
		require.Equal(t, `{"Bool":true,"Int":10,"PBool":true,"PInt":10,"PString":"abc","String":"abc"}`, dummyStr)
	})

	t.Run("#Stringify should omit nil values", func(t *testing.T) {
		type Dummy struct {
			Int int
			String string
			Bool bool
			PInt *int
			PString *string
			PBool *bool
		}

		i := 10
		s := "abc"
		b := true

		dummy := Dummy{
			Int: i,
			String: s,
			Bool: b,
			PInt: nil,
			PString: nil,
			PBool: nil,
		}

		dummyStr := Stringify(dummy)
		require.Equal(t, `{"Bool":true,"Int":10,"String":"abc"}`, dummyStr)
	})

	t.Run("#Stringify should stringify slice of strings", func(t *testing.T) {
		strings := []string{
			"first_string",
			"second_string",
			"third_string",
		}

		stringsStr := Stringify(strings)
		require.Equal(t, `[first_string second_string third_string]`, stringsStr)
	})

	t.Run("#Stringify should stringify slice of objects", func(t *testing.T) {
		type Dummy struct {
			Int int
			String string
			Bool bool
		}

		dummies := []Dummy{
			{Int: 1, String: "a", Bool: true},
			{Int: 2, String: "b", Bool: false},
		}

		dummiesStr := Stringify(dummies)
		require.Equal(t, `[{1 a true} {2 b false}]`, dummiesStr)
	})

	t.Run("#Stringify should stringify string map", func(t *testing.T) {
		stringMap := map[string]string{
			"key1": "value1",
			"key2": "value2",
		}

		stringMapStr := Stringify(stringMap)
		require.Equal(t, `{"key1":"value1","key2":"value2"}`, stringMapStr)
	})

	t.Run("#Parse should parse string into object", func(t *testing.T) {
		type Dummy struct {
			Int int
			String string
			Bool bool
			PInt *int
			PString *string
			PBool *bool
		}

		s := `{"Int": 5, "String": "abc", "Bool": true, "PInt": 10, "PString": "def", "PBool": false}`
		var obj Dummy
		Parse(s, &obj)

		require.Equal(t, 5, obj.Int)
		require.Equal(t, "abc", obj.String)
		require.Equal(t, true, obj.Bool)
		require.Equal(t, 10, *obj.PInt)
		require.Equal(t, "def", *obj.PString)
		require.Equal(t, false, *obj.PBool)
	})
}