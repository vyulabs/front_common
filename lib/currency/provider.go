package currency

import (
	"bitbucket.org/vyulabs/front_common/lib/interfaces"
	"encoding/json"
	"sort"
)

type currencyData struct {
	Symbol			string		`json:"symbol"`
	Name			string		`json:"name"`
	SymbolNative	string		`json:"symbol_native"`
	DecimalDigits	int			`json:"decimal_digits"`
	Rounding		float32		`json:"rounding"`
	Code			string		`json:"code"`
	NamePlural		string		`json:"name_plural"`
}

type CurrencyProvider struct {
	CurrencyMap		map[string]*interfaces.Currency
	CurrencyList	[]*interfaces.Currency
}

func NewCurrencyProvider() (*CurrencyProvider, error) {
	decodedMap := map[string]*currencyData{}

	currencyBytes := []byte(CurrencyData)
	currencyDecodeErr := json.Unmarshal(currencyBytes, &decodedMap)
	if currencyDecodeErr != nil {
		return nil, currencyDecodeErr
	}

	currencyCodeMap := map[string]*interfaces.Currency{}
	currencyNameMap := map[string]*interfaces.Currency{}
	currencyNames := []string{}
	for _, curr := range decodedMap {
		currency := &interfaces.Currency{
			Code:			curr.Code,
			Symbol:			curr.Symbol,
			Name:			curr.Name,
			SymbolNative: 	curr.SymbolNative,
			DecimalDigits: 	curr.DecimalDigits,
			Rounding:		curr.Rounding,
			NamePlural:		curr.NamePlural,
		}
		currencyCodeMap[currency.Code] = currency
		currencyNameMap[currency.Name] = currency
		currencyNames = append(currencyNames, currency.Name)
	}

	sort.Strings(currencyNames)
	currencyList := []*interfaces.Currency{}
	for _, currencyName := range currencyNames {
		currencyList = append(currencyList, currencyNameMap[currencyName])
	}

	return &CurrencyProvider{
		CurrencyMap: currencyCodeMap,
		CurrencyList: currencyList,
	}, nil
}

func (rc CurrencyProvider) GetCurrencies() []*interfaces.Currency {
	return rc.CurrencyList
}

func (rc CurrencyProvider) GetCurrency(currencyId string) *interfaces.Currency {
	return rc.CurrencyMap[currencyId]
}