package utils

import (
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

func TestTime(t *testing.T) {

	const TestTimeString = "2021-07-30T10:00:00Z"
	const TestDateString = "2021-08-31"

	t.Run("#StringToTime and #TimeToString call should return original string", func(t *testing.T) {
		srcTestTimeString := TestTimeString
		testTime, _ := StringToTime(srcTestTimeString)
		dstTestTimeString := TimeToString(testTime)

		require.Equal(t, srcTestTimeString, dstTestTimeString)
	})

	t.Run("#StringToTime and #TimeToDateString call should return original string", func(t *testing.T) {
		srcTestDateString := TestDateString
		testTime, _ := DateStringToTime(srcTestDateString)
		dstTestDateString := TimeToDateString(testTime)

		require.Equal(t, srcTestDateString, dstTestDateString)
	})

	t.Run("#StringToTimePointer should return nil if time string is invalid", func(t *testing.T) {
		require.Equal(t, (*time.Time)(nil), StringToTimePointer("invalid_time_string"))
	})

	t.Run("#StartOfDay should return start of day", func(t *testing.T) {
		testTime, _ := StringToTime(TestTimeString)
		startOfDay := StartOfDay(testTime)

		require.Equal(t, "2021-07-30T00:00:00Z", TimeToString(startOfDay))
	})

	t.Run("#StartOfMonth should return start of month", func(t *testing.T) {
		testTime, _ := StringToTime(TestTimeString)
		startOfMonth := StartOfMonth(testTime)

		require.Equal(t, "2021-07-01T00:00:00Z", TimeToString(startOfMonth))
	})

	t.Run("#StartOfYear should return start of year", func(t *testing.T) {
		testTime, _ := StringToTime(TestTimeString)
		startOfYear := StartOfYear(testTime)

		require.Equal(t, "2021-01-01T00:00:00Z", TimeToString(startOfYear))
	})
}
