package utils

import (
	"encoding/json"
	"fmt"
	"reflect"
)

func Stringify(obj any) string {
	kind := reflect.TypeOf(obj).Kind()
	if kind == reflect.Slice {
		return fmt.Sprintf("%v", obj)
	}

	return stringifyObj(obj)
}

func Parse(s string, obj any) error {
	return json.Unmarshal([]byte(s), obj)
}

//
// utils
//

func stringifyObj(obj any) string {
	marshaledObj, _ := json.Marshal(obj)
	mapped := make(map[string]any)
	json.Unmarshal(marshaledObj, &mapped)
	for key, val := range mapped {
		if val == nil {
			delete(mapped, key)
		}
	}
	marshaledMap, _ := json.Marshal(mapped)
	return string(marshaledMap)
}
