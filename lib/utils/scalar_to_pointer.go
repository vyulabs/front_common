package utils

func BoolPointer(f bool) *bool {
	boolValue := f
	return &boolValue
}

func IntPointer(n int) *int {
	intValue := n
	return &intValue
}

func Int32Pointer(n int32) *int32 {
	intValue := n
	return &intValue
}

func Int64Pointer(n int64) *int64 {
	intValue := n
	return &intValue
}

func Float32Pointer(x float32) *float32 {
	floatValue := x
	return &floatValue
}

func Float64Pointer(x float64) *float64 {
	floatValue := x
	return &floatValue
}

func StringPointer(s string) *string {
	stringValue := s
	return &stringValue
}