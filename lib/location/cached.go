package location

import (
	"bitbucket.org/vyulabs/front_common/lib/interfaces"
	"bitbucket.org/vyulabs/front_common/lib/utils"
	"strings"
)

type CachedLocationProvider struct {
	DirectProvider 	interfaces.LocationProvider
	Cache 			interfaces.ObjectCache
}

const CacheKeyPrefix = "ip-info-"

func NewCachedLocationProvider(
	directProvider interfaces.LocationProvider,
	cache interfaces.ObjectCache,
) *CachedLocationProvider {

	return &CachedLocationProvider{
		Cache: 			cache,
		DirectProvider: directProvider,
	}
}

func (rc CachedLocationProvider) GetLocationForIpAddress(ipAddress string) (*interfaces.Location, error) {
	if ipAddress == "" {
		return nil, nil
	}

	cleanIpAddress := utils.RemovePortFromIpAddress(ipAddress)

	cacheKey := getCacheKey(cleanIpAddress)
	hasCached, hasCachedErr := rc.Cache.Has(cacheKey)
	if hasCachedErr != nil {
		return nil, hasCachedErr
	}
	if *hasCached {
		var cachedLocation interfaces.Location
		getErr := rc.Cache.Get(cacheKey, &cachedLocation)
		if getErr != nil {
			return nil, getErr
		}
		return &cachedLocation, nil
	}

	location, locationErr := rc.DirectProvider.GetLocationForIpAddress(cleanIpAddress)
	if locationErr != nil {
		return nil, locationErr
	}

	saveErr := rc.Cache.Save(cacheKey, &location)
	if saveErr != nil {
		return nil, saveErr
	}

	return location, nil
}

func getCacheKey(ipAddress string) string {
	ipParts := strings.Split(ipAddress, ".")
	baseIp := strings.Join(ipParts[0:3], ".") // take first 3 numbers
	return CacheKeyPrefix + baseIp
}
