package cache

import (
	"context"
	"encoding/json"
	"github.com/go-redis/redis/v8"
	"time"
)

const (
	DefaultExpirationSeconds = 3600
	DefaultCtxTimeoutSeconds = 10
)

type RedisObjectCacheOptions struct {
	ExpirationSeconds int64
	CtxTimeoutSeconds int64
}

type RedisObjectCache struct {
	Redis   *redis.Client
	Options RedisObjectCacheOptions
}

func NewRedisObjectCache(redis *redis.Client, options RedisObjectCacheOptions) *RedisObjectCache {
	if options.ExpirationSeconds == 0 {
		options.ExpirationSeconds = DefaultExpirationSeconds
	}
	if options.CtxTimeoutSeconds == 0 {
		options.CtxTimeoutSeconds = DefaultCtxTimeoutSeconds
	}

	return &RedisObjectCache{
		Redis:   redis,
		Options: options,
	}
}

func (rc *RedisObjectCache) Has(key string) (*bool, error) {
	ctx, cancel := context.WithTimeout(
		context.Background(),
		time.Duration(rc.Options.CtxTimeoutSeconds)*time.Second,
	)
	defer cancel()

	_, err := rc.Redis.Get(ctx, key).Result()
	if err != nil {
		if err == redis.Nil {
			f := false
			return &f, nil
		}
		return nil, err
	}
	f := true
	return &f, nil
}

func (rc *RedisObjectCache) Get(key string, obj any) error {
	ctx, cancel := context.WithTimeout(
		context.Background(),
		time.Duration(rc.Options.CtxTimeoutSeconds)*time.Second,
	)
	defer cancel()

	value, valueErr := rc.Redis.Get(ctx, key).Result()
	if valueErr != nil {
		return valueErr
	}

	unmarshalErr := json.Unmarshal([]byte(value), obj)
	if unmarshalErr != nil {
		return unmarshalErr
	}
	return nil
}

func (rc *RedisObjectCache) Save(key string, obj any) error {
	buffer, bufferErr := json.Marshal(obj)
	if bufferErr != nil {
		return bufferErr
	}
	value := string(buffer)

	ctx, cancel := context.WithTimeout(
		context.Background(),
		time.Duration(rc.Options.CtxTimeoutSeconds)*time.Second,
	)
	defer cancel()

	rc.Redis.SetEX(ctx, key, value, time.Duration(rc.Options.ExpirationSeconds)*time.Second)

	return nil
}
