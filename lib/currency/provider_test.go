package currency

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestCurrencyProvider(t *testing.T) {

	t.Run("#NewCurrencyProvider should create new currency provider", func(t *testing.T) {
		_, cpErr := NewCurrencyProvider()
		require.Equal(t, nil, cpErr)
	})

	t.Run("#GetCurrency should get currency", func(t *testing.T) {
		cp, cpErr := NewCurrencyProvider()
		require.Equal(t, nil, cpErr)

		currency := cp.GetCurrency("USD")
		require.NotEqual(t, nil, currency)
		require.Equal(t, "USD", currency.Code)
	})
}
