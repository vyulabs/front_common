package interfaces

type Currency struct {
	Code 			string
	Symbol			string
	Name			string
	SymbolNative	string
	DecimalDigits	int
	Rounding		float32
	NamePlural		string
}

type CurrencyProvider interface {
	GetCurrencies() []*Currency
	GetCurrency(currencyId string) *Currency
}
