package utils

import (
	"github.com/stretchr/testify/require"
	"go.mongodb.org/mongo-driver/bson"
	"testing"
)

func TestUtils(t *testing.T) {

	t.Run("#CreateUuid should create UUID", func(t *testing.T) {
		uuid := CreateUuid()
		require.Equal(t, 36, len(uuid))
	})

	t.Run("#IsInterfaceEmpty should return true if interface is empty", func(t *testing.T) {
		isInterfaceEmpty, _ := IsInterfaceEmpty(bson.M{})
		require.Equal(t, true, isInterfaceEmpty)
	})

	t.Run("#IsInterfaceEmpty should return false if interface is not empty", func(t *testing.T) {
		isInterfaceEmpty, _ := IsInterfaceEmpty(bson.M{"field": "value"})
		require.Equal(t, false, isInterfaceEmpty)
	})
}