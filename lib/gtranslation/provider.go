package gtranslation

import (
	"bitbucket.org/vyulabs/front_common/lib/utils"
	"cloud.google.com/go/translate"
	"context"
	"golang.org/x/text/language"
	"google.golang.org/api/option"
	"log"
	"os"
)

type GoogleTranslationProvider struct {
	client 	*translate.Client
	log		*log.Logger
}

func NewGoogleTranslationProvider(apiKey string) (*GoogleTranslationProvider, error) {
	ctx := context.Background()
	client, clientErr := translate.NewClient(ctx, option.WithAPIKey(apiKey))
	if clientErr != nil {
		return nil, clientErr
	}

	log := log.New(os.Stderr, "GTranslate ", log.Lmsgprefix)

	return &GoogleTranslationProvider{
		client: client,
		log:	log,
	}, nil
}

func (rc GoogleTranslationProvider) GetTranslations(
	strings []string,
	srcLanguage language.Tag,
	dstLanguage language.Tag,
) ([]string, error) {
	texts := strings

	isTranslatable := false
	for _, s := range strings {
		if s != "" {
			isTranslatable = true
		}
	}

	if isTranslatable {
		ctx := context.Background()

		translations, transErr := rc.client.Translate(
			ctx,
			strings,
			dstLanguage,
			&translate.Options{
				Source: srcLanguage,
				Format: translate.Text,
			},
		)
		if transErr != nil {
			return nil, transErr
		}

		texts = []string{}
		for _, translation := range translations {
			texts = append(texts, translation.Text)
		}

		rc.log.Println(
			"GetTranslations - " + srcLanguage.String() + ":" + utils.Stringify(strings) + " -> " +
				dstLanguage.String() + ":" + utils.Stringify(texts),
		)
	}

	return texts, nil
}
