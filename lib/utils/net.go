package utils

import (
	"net/http"
	"strings"
)

func GetIpAddressForRequest(req *http.Request) string {
	ipAddress := req.RemoteAddr

	if req.Header != nil {
		xForwardedFor := req.Header.Get("x-forwarded-for")
		if xForwardedFor != "" {
			ipAddress = strings.Split(xForwardedFor, ",")[0]
		}
	}

	if ipAddress != "" {
		ipAddress = RemovePortFromIpAddress(ipAddress)
	}

	if IsLocalIpAddress(ipAddress) {
		return ""
	}

	return ipAddress
}

func RemovePortFromIpAddress(ipAddress string) string {
	lastColonPos := strings.LastIndex(ipAddress, ":")
	rightBracketPos := strings.LastIndex(ipAddress, "]")
	lastDotPos := strings.LastIndex(ipAddress, ".")
	if (
		rightBracketPos>-1 && lastColonPos > rightBracketPos ||
		rightBracketPos==-1 && lastColonPos > lastDotPos) {
		ipAddress = ipAddress[0:lastColonPos]
	}
	return ipAddress
}

func IsLocalIpAddress(ipAddress string) bool {
	return strings.Index(ipAddress, "127.0.0.1") > -1 ||
		strings.Index(ipAddress, "::1") > -1
}
