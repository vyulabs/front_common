package lib

import (
	"errors"
	"flag"
	"log"
	"os"
)

const SubcommandGenerateGraphqlSchema = "generate_graphql_schema"

func RunSubcommand() {
	switch os.Args[1] {

	case SubcommandGenerateGraphqlSchema:
		error := RunGenerateGraphqlSchemaSubcommand()
		if error != nil {
			log.Println("Error:", error.Error())
			os.Exit(1)
		}
	default:
		log.Println("Unknown subcommand:", os.Args[1])
		os.Exit(1)
	}
}

//
// Generate GraphQL schema tool
//
// Parameters example:
// generate_graphql_schema -source_dir /tmp/graphql/schema -target_file /tmp/schema.graphql
//

func RunGenerateGraphqlSchemaSubcommand() error {
	flagSet := flag.NewFlagSet(SubcommandGenerateGraphqlSchema, flag.ExitOnError)
	sourceDir := flagSet.String("source_dir", "", "source_dir")
	targetFile := flagSet.String("target_file", "", "target_file")
	flagSet.Parse(os.Args[2:])

	if *sourceDir == "" {
		return errors.New("source_dir not specified")
	}

	if *targetFile == "" {
		return errors.New("target_file not specified")
	}

	return GenerateGraphqlSchema(*sourceDir, *targetFile)
}

