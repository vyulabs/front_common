package mongo

import (
	"bitbucket.org/vyulabs/front_common/lib/db"
	"bitbucket.org/vyulabs/front_common/lib/utils"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type MongoDataSource struct {
	Name       string
	Collection *mongo.Collection
}

func (rc MongoDataSource) Count(filter map[string]any) (int64, error) {
	ctx, cancel := rc.getContext()
	defer cancel()

	return rc.Collection.CountDocuments(ctx, filter)
}

func (rc MongoDataSource) FindMany(
	filter map[string]any,
	output any,
	opts ...db.FindOptions,
) error {
	ctx, cancel := rc.getContext()
	defer cancel()

	var mongoOptions *options.FindOptions
	if len(opts) > 0 {
		var collation *options.Collation
		if opts[0].IgnoreCase != nil && *(opts[0].IgnoreCase) {
			collation = &options.Collation{
				Locale:   "en_US",
				Strength: 1,
			}
		}
		mongoOptions = &options.FindOptions{
			Projection: opts[0].Projection,
			Sort:       opts[0].Sort,
			Limit:      opts[0].Limit,
			Skip:       opts[0].Skip,
			Collation:  collation,
		}
	}
	res, err := rc.Collection.Find(ctx, filter, mongoOptions)
	if err != nil {
		return err
	}

	return res.All(ctx, output)
}

func (rc MongoDataSource) FindOne(
	filter map[string]any,
	output any,
	opts ...db.FindOptions,
) (bool, error) {
	ctx, cancel := rc.getContext()
	defer cancel()

	var mongoOptions options.FindOneOptions
	if len(opts) > 0 {
		mongoOptions = options.FindOneOptions{Projection: opts[0].Projection}
	}
	res := rc.Collection.FindOne(ctx, filter, &mongoOptions)
	err := res.Err()
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return false, nil
		}
		return false, err
	}

	return true, res.Decode(output)
}

func (rc MongoDataSource) FindOneById(
	id db.Id,
	output any,
	opts ...db.FindOptions,
) (bool, error) {
	ctx, cancel := rc.getContext()
	defer cancel()

	var mongoOptions options.FindOneOptions
	if len(opts) > 0 {
		mongoOptions = options.FindOneOptions{Projection: opts[0].Projection}
	}
	res := rc.Collection.FindOne(ctx, bson.M{"_id": ToMongoId(id)}, &mongoOptions)
	err := res.Err()
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return false, nil
		}
		return false, err
	}

	return true, res.Decode(output)
}

func (rc MongoDataSource) Exists(filter map[string]any) (bool, error) {
	ctx, cancel := rc.getContext()
	defer cancel()

	res := rc.Collection.FindOne(ctx, filter)
	err := res.Err()
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return false, nil
		}
		return false, err
	}

	return true, nil
}

func (rc MongoDataSource) ExistsById(id db.Id) (bool, error) {
	return rc.Exists(bson.M{"_id": ToMongoId(id)})
}

func (rc MongoDataSource) InsertOne(document any) error {
	ctx, cancel := rc.getContext()
	defer cancel()

	_, err := rc.Collection.InsertOne(ctx, document)
	return err
}

func (rc MongoDataSource) InsertMany(documents []any) error {
	if len(documents) == 0 {
		return nil
	}

	ctx, cancel := rc.getContext()
	defer cancel()

	_, err := rc.Collection.InsertMany(ctx, documents)
	return err
}

func (rc MongoDataSource) UpdateOneById(
	id db.Id,
	sets map[string]any,
	unsets map[string]any,
	output any,
	opts ...db.UpdateOptions,
) error {
	ctx, cancel := rc.getContext()
	defer cancel()

	setsEmpty, setsErr := utils.IsInterfaceEmpty(sets)
	if setsErr != nil {
		return setsErr
	}

	unsetsEmpty, unsetsErr := utils.IsInterfaceEmpty(unsets)
	if unsetsErr != nil {
		return unsetsErr
	}

	if !setsEmpty || !unsetsEmpty {
		updates := bson.M{}
		if !setsEmpty {
			updates["$set"] = sets
		}
		if !unsetsEmpty {
			updates["$unset"] = unsets
		}

		var mongoOptions *options.UpdateOptions
		if len(opts) > 0 {
			mongoOptions = &options.UpdateOptions{Upsert: &opts[0].Upsert}
		}

		_, err := rc.Collection.UpdateByID(ctx, ToMongoId(id), updates, mongoOptions)
		if err != nil {
			return err
		}
	} else { // only id specified
		exists, existsErr := rc.ExistsById(id)
		if existsErr != nil {
			return existsErr
		}
		if !exists { // create new empty record
			mongoId := ToMongoId(id)
			insertErr := rc.InsertOne(bson.M{"_id": mongoId})
			if insertErr != nil {
				return insertErr
			}
		}
	}

	if output == nil {
		return nil
	}

	_, findErr := rc.FindOneById(id, output)
	return findErr
}

func (rc MongoDataSource) UpdateMany(
	filter map[string]any,
	update map[string]any,
) (*db.UpdateResult, error) {
	ctx, cancel := rc.getContext()
	defer cancel()

	updateEmpty, updateErr := utils.IsInterfaceEmpty(update)
	if updateErr != nil {
		return nil, updateErr
	}

	if !updateEmpty {
		updateResult, updateErr := rc.Collection.UpdateMany(ctx, filter, update)
		if updateErr != nil {
			return nil, updateErr
		}
		return &db.UpdateResult{
			MatchedCount:  updateResult.MatchedCount,
			ModifiedCount: updateResult.ModifiedCount,
			UpsertedCount: updateResult.UpsertedCount,
			UpsertedId:    updateResult.UpsertedID,
		}, nil
	}

	return nil, nil
}

func (rc MongoDataSource) RemoveOneById(id db.Id) error {
	ctx, cancel := rc.getContext()
	defer cancel()

	_, err := rc.Collection.DeleteOne(ctx, bson.M{"_id": ToMongoId(id)})
	return err
}

func (rc MongoDataSource) RemoveMany(filter map[string]any) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := rc.Collection.DeleteMany(ctx, filter)
	return err
}

func (rc MongoDataSource) Distinct(
	fieldName string,
	filter map[string]any,
) (any, error) {
	ctx, cancel := rc.getContext()
	defer cancel()

	return rc.Collection.Distinct(ctx, fieldName, filter)
}

func (rc MongoDataSource) Aggregate(pipeline any, result any) error {
	ctx, cancel := rc.getContext()
	defer cancel()

	cursor, err := rc.Collection.Aggregate(ctx, pipeline)
	if err != nil {
		return err
	}
	return cursor.All(ctx, result)
}

//
// Utils
//

func (rc MongoDataSource) getContext() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), 10*time.Second)
}
