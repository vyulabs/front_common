package region

import (
	"bitbucket.org/vyulabs/front_common/lib/interfaces"
	"encoding/json"
	"sort"
)

type countryData struct {
	Name 				string 			`json:"countryName"`
	Code 				string          `json:"countryShortCode"`
	Regions				[]regionData 	`json:"regions"`
}

type regionData struct {
	Name				string			`json:"name"`
	Code				string			`json:"shortCode"`
}

type phoneData struct {
	Name				string			`json:"name"`
	Code				string			`json:"code"`
	PhoneCode			string			`json:"dial_code"`
}

type DictionaryRegionProvider struct {
	CountryMap			map[string]*interfaces.Country
	PhoneCodeSet		map[string]bool
}

func NewDictionaryRegionProvider() (*DictionaryRegionProvider, error) {
	countryMap := map[string]*interfaces.Country{}
	phoneCodeSet := map[string]bool{}

	countryBytes := []byte(CountryRegionData)
	var countries []countryData
	countryDecodeErr := json.Unmarshal(countryBytes, &countries)
	if countryDecodeErr != nil {
		return nil, countryDecodeErr
	}
	for _, country := range countries {
		regionMap := map[string]*interfaces.Region{}
		for _, region := range country.Regions {
			regionMap[region.Code] = &interfaces.Region{
				Name: region.Name,
				Code: region.Code,
			}
		}
		countryMap[country.Code] = &interfaces.Country {
			Name: country.Name,
			Code: country.Code,
			RegionMap: regionMap,
		}
	}

	phoneBytes := []byte(CountryPhoneData)
	var phones []phoneData
	phoneDecodeErr := json.Unmarshal(phoneBytes, &phones)
	if phoneDecodeErr != nil {
		return nil, phoneDecodeErr
	}
	for _, phone := range phones {
		country := countryMap[phone.Code]
		if country != nil {
			country.PhoneCode = phone.PhoneCode
			phoneCodeSet[phone.PhoneCode] = true
		}
	}

	return &DictionaryRegionProvider{
		CountryMap: countryMap,
		PhoneCodeSet: phoneCodeSet,
	}, nil
}

func (rc DictionaryRegionProvider) GetCountries() []*interfaces.Country {
	var countries []*interfaces.Country
	for _, country := range rc.CountryMap {
		countries = append(countries, country)
	}

	sort.Slice(countries, func(i, j int) bool {
		return countries[j].Name > countries[i].Name
	})

	return countries
}

func (rc DictionaryRegionProvider) GetCountry(countryCode string) *interfaces.Country {
	return rc.CountryMap[countryCode]
}

func (rc DictionaryRegionProvider) GetCountryName(countryCode string) *string {
	country := rc.CountryMap[countryCode]
	if country == nil {
		return nil
	}

	return &country.Name
}

func (rc DictionaryRegionProvider) GetCountryPhoneCode(countryCode string) *string {
	country := rc.CountryMap[countryCode]
	if country == nil {
		return nil
	}

	return &country.PhoneCode
}

func (rc DictionaryRegionProvider) GetRegion(countryCode string, regionCode string) *interfaces.Region {
	country := rc.GetCountry(countryCode)
	if country == nil {
		return nil
	}

	return country.RegionMap[regionCode]
}

func (rc DictionaryRegionProvider) GetRegionName(countryCode string, regionCode string) *string {
	country := rc.GetCountry(countryCode)
	if country == nil {
		return nil
	}

	region := country.RegionMap[regionCode]
	if region == nil {
		return nil
	}

	return &region.Name
}

func (rc DictionaryRegionProvider) SplitPhoneNumber(phoneNumber string) (string, string) {
	code := phoneNumber
	for code != "" {
		if rc.PhoneCodeSet[code] {
			break
		}
		code = code[:len(code)-1]
	}
	return code, phoneNumber[len(code):]
}