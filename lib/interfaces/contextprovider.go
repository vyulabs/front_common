package interfaces

import "net/http"

type ContextProvider interface {
	SaveContext(r *http.Request) (*http.Request, error)
}

