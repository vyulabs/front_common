package utils

import "time"

// https://golang.org/src/time/format.go
const TimeDateLayout = "2006-01-02"

func StringToTime(s string) (time.Time, error) {
	return time.Parse(time.RFC3339, s)
}

func StringToTimePointer(s string) *time.Time {
	t, tErr := StringToTime(s)
	if tErr != nil {
		return nil
	}
	return &t
}

func TimeToString(t time.Time) string {
	return t.Format(time.RFC3339)
}

func DateStringToTime(s string) (time.Time, error) {
	return time.Parse(TimeDateLayout, s)
}

func TimeToDateString(t time.Time) string {
	return t.Format(TimeDateLayout)
}

func StartOfDay(t time.Time) time.Time {
	year, month, day := t.Date()
	location := t.Location()
	return time.Date(year, month, day, 0, 0, 0, 0, location)
}

func StartOfMonth(t time.Time) time.Time {
	year, month, _ := t.Date()
	location := t.Location()
	return time.Date(year, month, 1, 0, 0, 0, 0, location)
}

func StartOfYear(t time.Time) time.Time {
	year, _, _ := t.Date()
	location := t.Location()
	return time.Date(year, 1, 1, 0, 0, 0, 0, location)
}
