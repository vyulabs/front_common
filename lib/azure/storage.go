package azure

import (
	"bitbucket.org/vyulabs/front_common/lib/utils"
	"context"
	"errors"
	"fmt"
	"github.com/Azure/azure-storage-blob-go/azblob"
	"mime"
	"net/url"
	"path/filepath"
)

func (rc *Azure) UploadBuffer(
	buffer []byte,
	bucket string,
	path string,
) error {
	rc.log.Println("Azure.UploadBuffer - bucket:", bucket, ", path:", path)

	ctx := context.Background()

	containerUrl, containerUrlErr := rc.newContainerUrl(bucket)
	if containerUrlErr != nil {
		return containerUrlErr
	}

	blockBlobUrl := containerUrl.NewBlockBlobURL(path)

	headers := azblob.BlobHTTPHeaders{
		ContentType: mime.TypeByExtension(filepath.Ext(path)),
	}
	rc.log.Println("headers:", utils.Stringify(headers))

	_, uploadErr := azblob.UploadBufferToBlockBlob(
		ctx,
		buffer,
		blockBlobUrl,
		azblob.UploadToBlockBlobOptions{
			BlockSize:   1024 * 1024,
			Parallelism: 16,
			BlobHTTPHeaders: headers,
		},
	)
	return uploadErr
}

func (rc *Azure) RemoveDir(bucket string, dir string) error {
	rc.log.Println("Azure.RemoveDir - bucket:", bucket, ", dir:", dir)

	// don't delete whole bucket contents!
	if dir == "" {
		return errors.New("Attempt to delete whole bucket content!")
	}

	fileNames, listErr := rc.ListFiles(bucket, dir)
	if listErr != nil {
		return listErr
	}

	for _, fileName := range fileNames {
		removeErr := rc.RemoveFile(bucket, fileName)
		if removeErr != nil {
			return removeErr
		}
	}

	return nil
}

func (rc *Azure) RemoveFile(bucket string, path string) error {
	rc.log.Println("Azure.RemoveFile - bucket:", bucket, "path:", path)

	ctx := context.Background()

	containerUrl, containerUrlErr := rc.newContainerUrl(bucket)
	if containerUrlErr != nil {
		return containerUrlErr
	}

	blockBlobUrl := containerUrl.NewBlockBlobURL(path)
	_, deleteErr := blockBlobUrl.Delete(
		ctx,
		azblob.DeleteSnapshotsOptionInclude,
		azblob.BlobAccessConditions{},
	)
	if deleteErr != nil {
		return deleteErr
	}

	return nil
}

func (rc *Azure) ListFiles(bucket string, path string) ([]string, error) {
	rc.log.Println("Azure.ListFiles - bucket:", bucket, ", path:", path)

	ctx := context.Background()

	containerUrl, containerUrlErr := rc.newContainerUrl(bucket)
	if containerUrlErr != nil {
		return nil, containerUrlErr
	}

	// https://gist.github.com/gauravdave01/97046fb686cfd6ad2629855264349ce1
	var fileNames []string
	for blobMarker := (azblob.Marker{}); blobMarker.NotDone(); {
		listResponse, err := containerUrl.ListBlobsFlatSegment(
			ctx,
			blobMarker,
			azblob.ListBlobsSegmentOptions{
				Prefix: path,
			})
		if err != nil {
			return nil, err
		}

		blobMarker = listResponse.NextMarker

		for _, blobItem := range listResponse.Segment.BlobItems {
			fileNames = append(fileNames, blobItem.Name)
		}
	}

	return fileNames, nil
}

func (rc *Azure) newContainerUrl(bucket string) (*azblob.ContainerURL, error) {
	accountName := rc.Config.StorageAccount
	accountKey := rc.Config.StorageAccessKey
	credential, credentialErr := azblob.NewSharedKeyCredential(accountName, accountKey)
	if credentialErr != nil {
		return nil, credentialErr
	}

	pipeline := azblob.NewPipeline(credential, azblob.PipelineOptions{})

	urlObj, urlError := url.Parse(fmt.Sprintf("https://%s.blob.core.windows.net/%s", accountName, bucket))
	if urlError != nil {
		return nil, urlError
	}

	containerUrl := azblob.NewContainerURL(*urlObj, pipeline)
	return &containerUrl, nil
}
