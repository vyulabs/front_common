package random

// https://stackoverflow.com/questions/22892120/how-to-generate-a-random-string-of-a-fixed-length-in-go

import (
	"errors"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

var runes = []rune("0123456789abcdefghijklmnopqrstuvwxyz")

func RandomString(stringLen int, runeSetLen int) (string, error) {
	if runeSetLen > len(runes) {
		return "", errors.New("runeSetLength is too big")
	}

	b := make([]rune, stringLen)
	for i := range b {
		b[i] = runes[rand.Intn(runeSetLen)]
	}
	return string(b), nil
}
