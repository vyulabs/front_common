package location

import (
	"bitbucket.org/vyulabs/front_common/lib/interfaces"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

type IpstackLocation struct {
	Ip 				string			`json:"ip"`
	CountryCode		string			`json:"country_code"`
	CountryName		string			`json:"country_name"`
	RegionCode		string			`json:"region_code"`
	RegionName		string			`json:"region_name"`
	City			string			`json:"city"`
	Zip				string			`json:"zip"`
	Latitude 		float64			`json:"latitude"`
	Longitude 		float64			`json:"longitude"`
}

type IpstackLocationProvider struct {
	AccessKey 	string
	log			*log.Logger
}

const ServiceUrl = "http://api.ipstack.com"

func NewIpstackLocationProvider(accessKey string) *IpstackLocationProvider {
	log := log.New(os.Stderr, "IPStack ", log.Lmsgprefix)

	return &IpstackLocationProvider{
		AccessKey: 	accessKey,
		log:		log,
	}
}

func (rc IpstackLocationProvider) GetLocationForIpAddress(ipAddress string) (*interfaces.Location, error) {
	requestUrl :=
		ServiceUrl + "/" + ipAddress + "?access_key=" + rc.AccessKey +
		"&fields=ip,country_code,country_name,region_code,region_name,city,zip,latitude,longitude"

	resp, respErr := http.Get(requestUrl)
	defer resp.Body.Close()

	if respErr != nil {
		return nil, respErr
	}
	if resp.StatusCode != 200 {
		return nil, errors.New("ipstack request failed")
	}

	body, bodyErr := ioutil.ReadAll(resp.Body)
	if bodyErr != nil {
		return nil, bodyErr
	}

	rc.log.Println("Body:", string(body))

	var ipstackLocation IpstackLocation
	decodeErr := json.Unmarshal(body, &ipstackLocation)
	if decodeErr != nil {
		return nil, decodeErr
	}

	location := convertIpstackLocation(ipstackLocation)
	return &location, nil
}

func convertIpstackLocation(location IpstackLocation) interfaces.Location {
	return interfaces.Location{
		Ip: location.Ip,
		CountryCode: location.CountryCode,
		CountryName: location.CountryName,
		RegionCode: location.RegionCode,
		RegionName: location.RegionName,
		City: location.City,
		Zip: location.Zip,
		Latitude: location.Latitude,
		Longitude: location.Longitude,
	}
}


