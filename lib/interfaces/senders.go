package interfaces

type Sms struct {
	Phone	string
	Text	string
}

type SmsSender interface {
	SendSms(sms *Sms) error
	SendSmsesAsync(smses []*Sms)
}

type Email struct {
	From	string
	To		string
	Subject	string
	Text	string
	Html	string
}

type EmailSender interface {
	SendEmail(email *Email) error
	SendEmailsAsync(emails []*Email)
}