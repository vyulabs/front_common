package interfaces

type ObjectCache interface {
	Has(key string) (*bool, error)
	Get(key string, obj any) error
	Save(key string, obj any) error
}
