package utils

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestNet(t *testing.T) {

	t.Run("#RemovePortFromIpAddress should remove port from IP address", func(t *testing.T) {

		require.Equal(t, "1.2.3.4", RemovePortFromIpAddress("1.2.3.4:80"))
		require.Equal(t, "1.2.3.4", RemovePortFromIpAddress("1.2.3.4"))

		require.Equal(t, "::ffff:1.2.3.4", RemovePortFromIpAddress("::ffff:1.2.3.4:80"))
		require.Equal(t, "::ffff:1.2.3.4", RemovePortFromIpAddress("::ffff:1.2.3.4"))

		require.Equal(t, "[::1]", RemovePortFromIpAddress("[::1]:80"))
		require.Equal(t, "[::1]", RemovePortFromIpAddress("[::1]"))

		require.Equal(t, "www.domain.com", RemovePortFromIpAddress("www.domain.com:8080"))
		require.Equal(t, "www.domain.com", RemovePortFromIpAddress("www.domain.com"))
	})

	t.Run("#IsLocalIpAddress should return true if IP address is local", func(t *testing.T) {
		require.Equal(t, false, IsLocalIpAddress("1.2.3.4:80"))
		require.Equal(t, false, IsLocalIpAddress("1.2.3.4"))

		require.Equal(t, false, IsLocalIpAddress("::ffff:1.2.3.4:80"))
		require.Equal(t, false, IsLocalIpAddress("::ffff:1.2.3.4"))

		require.Equal(t, true, IsLocalIpAddress("127.0.0.1:80"))
		require.Equal(t, true, IsLocalIpAddress("127.0.0.1"))

		require.Equal(t, true, IsLocalIpAddress("::ffff:127.0.0.1:80"))
		require.Equal(t, true, IsLocalIpAddress("::ffff:127.0.0.1"))

		require.Equal(t, true, IsLocalIpAddress("[::1]:80"))
		require.Equal(t, true, IsLocalIpAddress("[::1]"))

		require.Equal(t, false, IsLocalIpAddress("www.domain.com:8080"))
		require.Equal(t, false, IsLocalIpAddress("www.domain.com"))
	})
}
