package db

type Id struct {
	Id         string
	IsObjectId bool
}

func NewStringId(newId string) Id {
	return Id{
		Id:         newId,
		IsObjectId: false,
	}
}

func NewObjectId(newId string) Id {
	return Id{
		Id:         newId,
		IsObjectId: true,
	}
}
