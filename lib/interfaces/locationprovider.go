package interfaces

type Location struct {
	Ip 				string			`json:"ip"`
	CountryCode		string			`json:"country_code"`
	CountryName		string			`json:"country_name"`
	RegionCode		string			`json:"region_code"`
	RegionName		string			`json:"region_name"`
	City			string			`json:"city"`
	Zip				string			`json:"zip"`
	Latitude 		float64			`json:"latitude"`
	Longitude 		float64			`json:"longitude"`
}

type LocationProvider interface {
	GetLocationForIpAddress(ipAddress string) (*Location, error)
}